package ru.arubtsova.tm.api;

import ru.arubtsova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
